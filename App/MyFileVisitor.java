package GIU.reworked;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.BiFunction;

public class MyFileVisitor extends SimpleFileVisitor<Path>{
	
	private String fileName;
	private String fileLocation;
	private String currentFile;
	private Worker worker;
	private int count;
	
	private BiFunction<Path, BasicFileAttributes, FileVisitResult> evaluator = (path, attrs) -> {
		
		count++;
		worker.firePropertyChange("currentFile", currentFile, path.getFileName().toString());
		worker.firePropertyChange("fileCount", 0, count);
		if(path.getFileName().toString().toUpperCase().contains(fileName)) { 
			fileLocation = path.toAbsolutePath().toString();
			return FileVisitResult.TERMINATE;
		}
		if(attrs.isRegularFile()) {
			currentFile = path.getFileName().toString();
		}
		return FileVisitResult.CONTINUE;
	};
	
	
	public MyFileVisitor(Worker worker, String fileName) {
		this.worker = worker;
		this.fileName = fileName.toUpperCase();
	}
	
	public String getFileLocation() {
		return fileLocation;
	}
	
	public String getCurrentFile() {
		return currentFile;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return evaluator.apply(file, attrs);
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		
		return evaluator.apply(dir, attrs);
	}
	
}