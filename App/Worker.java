package GIU.reworked;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.function.BiConsumer;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

public class Worker extends SwingWorker<Void, Void>{
	
	private BiConsumer<Boolean, String> onDone;
	private MainFrame frame;
	
	public Worker(BiConsumer<Boolean, String> onDone, MainFrame frame) {
		this.onDone = onDone;
		this.frame = frame;
	}

	@Override
	protected Void doInBackground() throws IOException {
	
		MyFileVisitor mfv = new MyFileVisitor(this, frame.searchForFileField.getText());
		try{
			Files.walkFileTree(Paths.get(frame.dirField.getText()), mfv);
		} catch(IOException e) {
			JOptionPane.showMessageDialog(frame, "Invalid directory path.");
			return null;
		}
		
		if(Objects.nonNull(mfv.getFileLocation())) {
			JOptionPane.showMessageDialog(frame, "Wanted file is located at:\n" + mfv.getFileLocation());
		} else {
			JOptionPane.showMessageDialog(frame, "No such file in chosen directory.");
		}
		return null;
	}

	@Override
	public void done() {
		onDone.accept(true, "Last folder or file checked:");
	}

}
