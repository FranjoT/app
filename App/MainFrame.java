package GIU.reworked;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.function.BiConsumer;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;


public class MainFrame extends JFrame /*implements KeyListener*/{

	private static final long serialVersionUID = -8584834950528484699L;
	
	JLabel welcomeLabel = new JLabel("Welcome to the file searcher application!", SwingConstants.CENTER);	
	
	JLabel searchForFileLabel = new JLabel("Enter file you want to search for:", SwingConstants.RIGHT);
	JTextField searchForFileField = new JTextField();
	
	JLabel dirPathLabel = new JLabel("Enter directory path you want to start the search from: ", SwingConstants.RIGHT);
	JTextField dirField = new JTextField();
	
	JLabel currFileLabel = new JLabel("Current folder:", SwingConstants.RIGHT);
	JLabel changingFolderLabel = new JLabel("");

	JLabel countLabel = new JLabel("Number of files or folders checked:", SwingConstants.RIGHT);
	JTextField countField = new JTextField();
	
	JButton startButton = new JButton("Start");
	AbstractAction startAct = new AbstractAction() {

		private static final long serialVersionUID = -6808986686025027146L;

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				start();
			} catch (Exception exc) {
				JOptionPane.showMessageDialog(rootPane, exc.getMessage());
			}
			
		}
	};
	
	
	BiConsumer<Boolean, String> onDone = (flag, currFolderLabelText) -> {
		startButton.setEnabled(flag);
		searchForFileField.setEditable(flag);
		dirField.setEditable(flag);
		currFileLabel.setText("Last file checked:");
		this.requestFocusInWindow();
		};
	

	public MainFrame(String title) {

		super(title);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		
		initGUI();
		initListeners();
		initKeyBindings();
		

		// setSize(500, 190);
		pack();
		setVisible(true);
		setLocationRelativeTo(null);
		setResizable(false);
		this.requestFocusInWindow();
	}

	private void initListeners() {

		startButton.addActionListener(e -> {
			try {
				start();
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			}
		});
		
		/* ako se implementa KeyListener
		 * 
		 * this.addKeyListener(this);
		 */
		
		this.addKeyListener(new KeyAdapter(){
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					System.exit(0);
				}
			}
		});
	}
	
	private void initKeyBindings() {
		startButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "pressed");
		startButton.getActionMap().put("pressed", startAct);
	}

	private void initGUI() {

		((JPanel) getContentPane()).setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));	

		
		
		welcomeLabel.setFont(new Font("Bible Script", Font.BOLD, 17));	
		add(welcomeLabel, BorderLayout.NORTH);
		
		JPanel centerPanel = new JPanel(new GridLayout(0, 2, 10, 2));
		add(centerPanel, BorderLayout.CENTER);

		centerPanel.add(searchForFileLabel);
		centerPanel.add(searchForFileField);

		centerPanel.add(dirPathLabel);
		centerPanel.add(dirField);
		
		centerPanel.add(currFileLabel);
		centerPanel.add(changingFolderLabel);
		
		centerPanel.add(countLabel);
		countField.setEditable(false);
		countField.setBackground(Color.GRAY);
		countField.setForeground(Color.WHITE);
		countField.setFont(new Font("", Font.BOLD, 15));
		centerPanel.add(countField);
		
		
		JPanel southPan = new JPanel();
		add(southPan, BorderLayout.SOUTH);
		southPan.add(startButton);
		startButton.setFont(new Font("", Font.ITALIC, 12));
		
	}


	private void start() throws IOException {

		if (dirField.getText().isEmpty() || searchForFileField.getText().isEmpty()) {
			this.requestFocusInWindow();
			throw new IOException("Niti jedno polje nesmije biti prazno!");
		}

		countField.setText("0");
		onDone.accept(false, "Current folder:");

		Worker worker = new Worker(onDone, this);

		worker.addPropertyChangeListener(evt -> {
			if ("currentFile".equals(evt.getPropertyName())) {
				changingFolderLabel.setText(String.valueOf(evt.getNewValue()));
			} else if ("fileCount".equals(evt.getPropertyName())) {
				countField.setText(String.valueOf(evt.getNewValue()));
			}

		});

		worker.execute();
	}

/* ako se implementa KeyListener
 * 
 * 	@Override
 *	public void keyTyped(KeyEvent e) {
 *		// doNothing
 *	}
 *
 *	@Override
 *	public void keyPressed(KeyEvent e) {
 *		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
 *			System.exit(0);
 *		}
 *				
 *	}
 *
 *	@Override
 *	public void keyReleased(KeyEvent e) {
 *		// doNothing
 *	}
 */	
	

}
